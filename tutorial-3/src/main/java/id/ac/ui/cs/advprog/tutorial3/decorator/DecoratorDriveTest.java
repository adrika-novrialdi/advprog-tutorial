package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorDriveTest {

    public static void main(String[] args) {
        Food food = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        System.out.println(String.format("%s $%.2f",food.getDescription(),food.cost()));

        Food food1 = BreadProducer.THICK_BUN.createBreadToBeFilled();
        food1 = FillingDecorator.LETTUCE.addFillingToBread(food1);
        food1 = FillingDecorator.CHICKEN_MEAT.addFillingToBread(food1);
        food1 = FillingDecorator.CHEESE.addFillingToBread(food1);

        System.out.println(String.format("%s $%.2f",food1.getDescription(),food1.cost()));

    }
}
