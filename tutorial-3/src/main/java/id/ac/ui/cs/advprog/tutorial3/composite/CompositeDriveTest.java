package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.Iterator;

public class CompositeDriveTest {

    public static void main(String[] args) {
        Company mycompany = new Company();
        Ceo riana = new Ceo("Riana", 500000.00);
        mycompany.addEmployee(riana);
        printHelper(riana, mycompany);

        Cto raven = new Cto("Raven", 100000.00);
        mycompany.addEmployee(raven);
        printHelper(raven, mycompany);

        BackendProgrammer rigel = new BackendProgrammer("Rigel", 80000.00);
        mycompany.addEmployee(rigel);
        printHelper(rigel, mycompany);

        FrontendProgrammer flora = new FrontendProgrammer("Flora", 30000.00);
        mycompany.addEmployee(flora);
        printHelper(flora, mycompany);

        NetworkExpert affania = new NetworkExpert("Affania", 50000.00);
        mycompany.addEmployee(affania);
        printHelper(affania, mycompany);

        SecurityExpert wurrynyan = new SecurityExpert("Wurry-Nyan", 70000.00);
        mycompany.addEmployee(wurrynyan);
        printHelper(wurrynyan, mycompany);

        UiUxDesigner sanchan = new UiUxDesigner("San-chan", 90000.00);
        mycompany.addEmployee(sanchan);
        printHelper(sanchan, mycompany);


    }

    public static void printHelper(Employees employees, Company company) {
        System.out.println(String.format("Adding %s, as a %s in company",
                employees.getName(), employees.getRole()));
        Iterator<Employees> iterator = company.employeesList.iterator();
        System.out.println("Company now consists of: ");
        while (iterator.hasNext()) {
            Employees currEmployees = iterator.next();
            System.out.println(String.format("%s as a %s ",
                    currEmployees.getName(), currEmployees.getRole()));
        }
        System.out.println("===============");
    }
}
