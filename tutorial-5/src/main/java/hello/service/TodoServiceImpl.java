package hello.service;

import hello.model.TodoModel;
import hello.repository.TodoDb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class TodoServiceImpl implements TodoService {
    @Autowired
    private TodoDb todoDb;

    public TodoServiceImpl(TodoDb todoDb) {
        this.todoDb = todoDb;
    }

    @Override
    public List<TodoModel> findAll() {
        return todoDb.findAll();
    }

    @Override
    public TodoModel addTodo(TodoModel todoModel) {
        return todoDb.save(todoModel);
    }
}
