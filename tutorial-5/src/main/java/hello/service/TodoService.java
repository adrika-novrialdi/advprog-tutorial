package hello.service;

import hello.model.TodoModel;
import java.util.List;


public interface TodoService {
    List<TodoModel> findAll();

    TodoModel addTodo(TodoModel todoModel);
}