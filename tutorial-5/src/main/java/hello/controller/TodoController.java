package hello.controller;

import hello.model.TodoModel;
import hello.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TodoController {
    @Autowired
    private TodoService todoService;

    @RequestMapping(value = "/todo",method = RequestMethod.GET)
    public String todoHome(Model model) {
        model.addAttribute("todo",todoService.findAll());
        model.addAttribute("newTodo",new TodoModel());
        return "todo-home";
    }

    @RequestMapping(value = "/todo/add", method = RequestMethod.POST)
    public String addTodo(@ModelAttribute TodoModel todoModel,Model model) {
        todoService.addTodo(todoModel);
        model.addAttribute("newTodo",new TodoModel());
        model.addAttribute("todo",todoService.findAll());
        return "todo-home";
    }
}
