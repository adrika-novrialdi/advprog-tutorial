package hello.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @RequestMapping(value = "/cv", method = RequestMethod.GET)
    public String welcome(@RequestParam(name = "name",required = false)String name, Model model) {
        model.addAttribute("name",name);
        return "homeCV";
    }

}
