package hello.model.builder;

import hello.model.TodoModel;

public class TodoModelBuilder {
    private TodoModel todoModel;

    public TodoModelBuilder() {
        todoModel = new TodoModel();
    }

    public TodoModelBuilder id(long id) {
        todoModel.setId(id);
        return this;
    }

    public TodoModelBuilder description(String description) {
        todoModel.setDescription(description);
        return this;
    }

    public TodoModel build() {
        return todoModel;
    }
}
