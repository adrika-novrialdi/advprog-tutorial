package hello.repository;

import hello.model.TodoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoDb extends JpaRepository<TodoModel,Long> {
}
