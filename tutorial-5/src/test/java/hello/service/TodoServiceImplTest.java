package hello.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import hello.model.TodoModel;
import hello.model.builder.TodoModelBuilder;
import hello.repository.TodoDb;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import org.mockito.ArgumentCaptor;



public class TodoServiceImplTest {
    private TodoDb todoDbmock;

    private TodoService todoService;

    @Before
    public void setUp() {
        todoDbmock = mock(TodoDb.class);
        todoService = new TodoServiceImpl(todoDbmock);
    }

    @Test
    public void addNewTodoEntry() {
        TodoModel todoModel = new TodoModelBuilder()
                .id(1)
                .description("Finish test")
                .build();

        todoService.addTodo(todoModel);

        ArgumentCaptor<TodoModel> toDoArgument = ArgumentCaptor.forClass(TodoModel.class);
        verify(todoDbmock, times(1)).save(toDoArgument.capture());
        verifyNoMoreInteractions(todoDbmock);

        TodoModel model = toDoArgument.getValue();

        assertThat(model.getDescription(), is(todoModel.getDescription()));
    }

    @Test
    public void todoFindAll() {
        List<TodoModel> models = new ArrayList<>();
        when(todoDbmock.findAll()).thenReturn(models);

        List<TodoModel> actual = todoService.findAll();

        verify(todoDbmock, times(1)).findAll();
        verifyNoMoreInteractions(todoDbmock);

        assertThat(actual, is(models));

    }



}