package hello.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class TodoModelTest {

    @Test
    public void testTodoSet() {
        TodoModel todoModel = new TodoModel();
        todoModel.setId(1);
        todoModel.setDescription("Create test");

        assertEquals(1,todoModel.getId());
        assertEquals("Create test",todoModel.getDescription());
    }

    public void testTodoUpdate() {
        TodoModel todoModel = new TodoModel();
        todoModel.setId(1);
        todoModel.setDescription("Create test");

        todoModel.setId(2);
        todoModel.setDescription("Create tasks");

        assertNotEquals(1,todoModel.getId());
        assertNotEquals("Create test",todoModel.getDescription());

        assertEquals(2,todoModel.getId());
        assertEquals("Create tasks",todoModel.getDescription());
    }

}