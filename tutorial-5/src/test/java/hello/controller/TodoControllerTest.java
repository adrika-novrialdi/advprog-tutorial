package hello.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import hello.model.TodoModel;
import hello.model.builder.TodoModelBuilder;
import hello.service.TodoService;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;



@RunWith(SpringRunner.class)
@WebMvcTest(TodoController.class)
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoService todoService;

    @Test
    public void postTodo() throws Exception {
        TodoModel todoModel = new TodoModel();
        todoModel.setDescription("Finish test");

        mockMvc.perform(post("/todo/add")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .sessionAttr("todo",todoModel))
                .andExpect(status().isOk());
    }

    @Test
    public void todoIsOnThePage()throws Exception {
        TodoModel todoModel = new TodoModelBuilder()
                .id(1)
                .description("Finish test")
                .build();


        mockMvc.perform(get("/todo"))
                .andExpect(content().string(containsString("")));
    }

}