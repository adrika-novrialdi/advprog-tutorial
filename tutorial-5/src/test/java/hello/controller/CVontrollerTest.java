package hello.controller;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CvController.class)
public class CVontrollerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testCvPageWithName() throws Exception {
        String name = "Minato";
        mockMvc.perform(get("/cv").param("name", name))
                .andExpect(content().string(containsString(name
                        + ", I hope you interested to hire me")));
    }

    public void testCvPageWithNameNotReturnThisIs() throws Exception {
        String name = "Minato";
        mockMvc.perform(get("/cv").param("name",name))
                .andExpect(content().string(not("This is my CV")));
    }



}