package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class ManilaClams implements Clams {
    @Override
    public String toString() {
        return "Manila Clams";
    }
}
