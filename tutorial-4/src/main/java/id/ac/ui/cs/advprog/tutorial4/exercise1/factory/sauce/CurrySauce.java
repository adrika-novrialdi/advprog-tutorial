package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class CurrySauce implements Sauce {
    @Override
    public String toString() {
        return "Curry Sauce";
    }
}
