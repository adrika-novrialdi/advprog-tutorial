package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.FireCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.ManilaClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.WheatDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.CurrySauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Cauliflower;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.KabochaSquash;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Potato;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new WheatDough();
    }

    @Override
    public Sauce createSauce() {
        return new CurrySauce();
    }

    @Override
    public Cheese createCheese() {
        return new FireCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Potato(), new KabochaSquash(), new Cauliflower(), new Mushroom()};
        return veggies;
    }

    @Override
    public Clams createClam() {
        return new ManilaClams();
    }
}
