package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.spy;

import org.junit.Test;
import org.mockito.InOrder;

public class PizzaStoreTest {
    private PizzaStore pizzaStore;

    @Test
    public void testOrderPizzaDepokFollowsAlgorithm() {
        pizzaStore = spy(new DepokPizzaStore());

        InOrder inOrder = inOrder(pizzaStore);

        pizzaStore.orderPizza("cheese");
        inOrder.verify(pizzaStore).createPizza("cheese");
        inOrder.verifyNoMoreInteractions();

        pizzaStore.orderPizza("clam");
        inOrder.verify(pizzaStore).createPizza("clam");
        inOrder.verifyNoMoreInteractions();

        pizzaStore.orderPizza("veggie");
        inOrder.verify(pizzaStore).createPizza("veggie");
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testOrderPizzaNewYorkFollowAlgorithm() {
        pizzaStore = spy(new NewYorkPizzaStore());
        InOrder inOrder = inOrder(pizzaStore);

        pizzaStore.orderPizza("cheese");
        inOrder.verify(pizzaStore).createPizza("cheese");
        inOrder.verifyNoMoreInteractions();

        pizzaStore.orderPizza("clam");
        inOrder.verify(pizzaStore).createPizza("clam");
        inOrder.verifyNoMoreInteractions();

        pizzaStore.orderPizza("veggie");
        inOrder.verify(pizzaStore).createPizza("veggie");
        inOrder.verifyNoMoreInteractions();



    }
}